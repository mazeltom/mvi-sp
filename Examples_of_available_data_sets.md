Available data sets include:

[Training data](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/data/training_set_example.csv)

[Example test](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/data/example_test.csv)

[Lectures](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/data/lectures.csv)

[Questions](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/data/questions.csv)

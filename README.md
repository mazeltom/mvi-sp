# mvi-sp

Semester project

**Answer Correctness Prediction ([Kaggle competition](https://www.kaggle.com/c/riiid-test-answer-prediction))**

The challenge of this Kaggle competition is to create algorithms for "Knowledge Tracing", i.e. modeling of student knowledge over time. 
The goal is to accurately predict how students will perform on future interactions based on their past results and activity.

The algorithms will help tackle global challenges in education. If successful, it’s possible that any student with an Internet connection can enjoy the benefits of a personalized learning experience, regardless of where they live. With your participation, we can build a better and more equitable model for education in a post-COVID-19 world.

Submissions to the competition must be made through Notebooks.

The following conditions must be met:

CPU Notebook <= 9 hours run-time
GPU Notebook <= 9 hours run-time
TPU Notebook <= 3 hours run-time
Freely & publicly available external data is allowed, including pre-trained models
Submission file must be named submission.csv

Detailed description is available [here](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/task_description.pdf).

Examples of available data sets are available [here](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/edit/master/Examples_of_available_data_sets.md).

Jupyter notebooks used are available [here](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/edit/master/notebooks).

**Report** and more details are available [here](https://gitlab.fit.cvut.cz/mazeltom/mvi-sp/blob/master/Report.pdf).